import{Injectable} from '@angular/core';
import{HttpClient, HttpHandler, HttpHeaders,HttpParams}from '@angular/common/http';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import{Attore} from '../entity/Attore';

@Injectable({
    providedIn:'root'
})
export class AttoreService{
    serverAddress='http://localhost:8080/api/attori'
    constructor (private http:HttpClient){    }

   
//oggetto di tipo form
private options = { headers: new HttpHeaders().set('Access-Control-Allow-Origin', '*') };

    public form: FormGroup = new FormGroup({
        codAttore: new FormControl(),
        nome: new FormControl('', Validators.required),
        annoNascita: new FormControl(2000),
        country: new FormControl('Italy')
    });

    initializeFormGroup(attore:Attore){
        this.form.setValue({
            codAttore:attore.codAttore,
            nome:attore.nome,
            annoNascita:attore.annoNascita,
            country:attore.country
        });
    }
    getAttori(){
        return this.http.get(this.serverAddress, this.options);
    }
    
    addAttore(attore:Attore){
        console.log(attore)
        return this.http.post(this.serverAddress,attore)
    }

    deleteAttore(codAttore:number){
        console.log(codAttore)
        return this.http.delete(this.serverAddress+"/"+codAttore, this.options).subscribe(() => console.log('Delete successful'));
    }

    getAttoriByID(codAttore:number) {
        return this.http.get(this.serverAddress+"/"+codAttore, this.options);
    }

    putAttore(attore:Attore){
        return this.http.put(this.serverAddress,attore);
    }
    


    
}
