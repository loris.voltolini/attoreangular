import { ComponentFixture, TestBed } from '@angular/core/testing';

import { InserimentoAttoreComponent } from './inserimento-attore.component';

describe('InserimentoAttoreComponent', () => {
  let component: InserimentoAttoreComponent;
  let fixture: ComponentFixture<InserimentoAttoreComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ InserimentoAttoreComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(InserimentoAttoreComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
