import { Component, OnInit } from '@angular/core';
import{Attore} from '../entity/Attore';
import{AttoreService} from '../services/attore-service';

@Component({
  selector: 'app-inserimento-attore',
  templateUrl: './inserimento-attore.component.html',
  styleUrls: ['./inserimento-attore.component.css']
})
export class InserimentoAttoreComponent implements OnInit {

  constructor(public attoriService:AttoreService) { }

  ngOnInit(): void {
  }
  
  onSubmit(){
    this.attoriService.addAttore(this.attoriService.form.value).subscribe(result => console.log(result));
  }
}
