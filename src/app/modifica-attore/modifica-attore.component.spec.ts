import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ModificaAttoreComponent } from './modifica-attore.component';

describe('ModificaAttoreComponent', () => {
  let component: ModificaAttoreComponent;
  let fixture: ComponentFixture<ModificaAttoreComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ModificaAttoreComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ModificaAttoreComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
